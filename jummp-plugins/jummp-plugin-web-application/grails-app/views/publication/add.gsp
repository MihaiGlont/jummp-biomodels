<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 2019-05-17
  Time: 20:09
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="net.biomodels.jummp.model.PublicationLinkProvider; grails.converters.JSON;" %>
<%
    def style = grailsApplication.config.jummp.branding.style
    def serverUrl = grailsApplication.config.grails.serverURL
%>
<html>
<head>
    <meta name="layout" content="biomodels/main" />
    <title>${title}</title>
    <link rel="stylesheet"
          href="${resource(contextPath: "${serverUrl}", dir: "/css/${style}", file: 'publicationPageStyle.css')}" />
    <link rel="stylesheet"
          href="${resource(contextPath: "${serverUrl}", dir: "css", file: 'toastr.min.css')}"/>

    <g:javascript>
        let authorMap = {"authors": []};
        let authorList = [];
    </g:javascript>
    <g:javascript contextPath="" src="${style}/publicationSubmission.js"/>
    <g:javascript src="toastr.min.js" contextPath=""/>
    <g:javascript>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </g:javascript>
</head>

<body>
    <div class="row">
        <h2>Add a new publication</h2>

        <g:render template="/templates/publication/refreshPubMedDataButton"
                  plugin="jummp-plugin-web-application"
                  model="['publication': publication]"/>
        <div id="publicationForm">
            <g:render template="/templates/publication/publicationEditableElements"
                      plugin="jummp-plugin-web-application"
                      model="['publication': publication, 'authorListContainerSize': authorListContainerSize]"/>
            <g:render template="/templates/publication/publicationButtonsForm"
                      plugin="jummp-plugin-web-application"/>
        </div>
    </div>
    <g:javascript>
        $('#btnSave').on("click", function(event) {
            "use strict";
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: $.jummp.createLink("publication", "save"),
                cache: true,
                processData: true,
                async: true,
                data: JSON.stringify(buildPublicationTC()),
                contentType: "application/json",
                beforeSend: function() {
                    toastr.info("The publication details are being saved. Please wait...");
                },
                success: function(response) {
                    toastr.clear();
                    if (response['status'] === 200) {
                        toastr.success(response['message']);
                        setTimeout(function() {
        window.location.href = "${createLink(controller: "publication", action: "show")}"+"/" + response['publicationId'];
                        }, 5200); // 5200 is equivalent to toastr.options.timeOut due to toastr.success

                    } else if (response['status'] === 500) {
                        toastr.error(response['message']);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.clear();
                    toastr.error("Error: ", jqXHR.responseText + textStatus + errorThrown + JSON.stringify(jqXHR));
                }
            });
        });
        /* build up a publication transport command object */
        function buildPublicationTC() {
            let linkProvider = {
                "linkType": $('#linkProvider').val(),
                "pattern": ""
            };
            let link = $('#link').val();
            let title = $('#title').val();
            let journal = $('#journal').val();
            let affiliation = $('#affiliation').val();
            let synopsis = $('#synopsis').val();
            let year = $('#year').val();
            let month = $('#month').val();
            let day = $('#day').val();
            let volume = $('#volume').val();
            let issue = $('#issue').val();
            let pages = $('#pages').val();
            let opts = $('#authorList option');
            let authors  = $.map(opts, function(opt) {
                if ($(opt).attr("data-person-id") === "undefined") {
                    return {
                        "userRealName": $(opt).attr("data-person-realname"),
                        "orcid": $(opt).attr("data-person-orcid"),
                        "institution": $(opt).attr("data-person-institution")
                    };
                } else {
                    return {
                        "id": $(opt).attr("data-person-id"),
                        "userRealName": $(opt).attr("data-person-realname"),
                        "orcid": $(opt).attr("data-person-orcid"),
                        "institution": $(opt).attr("data-person-institution")
                    };
                }
            });

            let pubCmd = {
                'linkProvider': linkProvider,
                'link': link,
                'title': title,
                'journal': journal,
                'affiliation': affiliation,
                'synopsis': synopsis,
                'year': year,
                'month': month,
                'day': day,
                'volume': volume,
                'issue': issue,
                'pages': pages,
                'authors': authors
            };
            return pubCmd;
        }

        $('input[name="Back"]').on("click", function() {
            // The following function is defined in publicationSubmission.js
            backAway();
        });

        $(document).on('click', '#refreshPublicationFromPubMed', {}, function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "${createLink(controller: "publication", action: "refreshPubMedData")}",
                data: {
                    pubmed: $('#link').val()
                }
            }).done(function(data) {
                $('.editablePart').html(data);
            });
        });
    </g:javascript>
</body>
</html>
