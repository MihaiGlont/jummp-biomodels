<%--
 Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>






<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta name="layout" content="${session['branding.style']}/main" />
    <style type="text/css">
        .underline {
            border-bottom: 1px solid grey;
        }
    </style>
</head>
<body>

<h2>Acknowledgements</h2>

BioModels is developed by the <a href="https://www.ebi.ac.uk/about/people/henning-hermjakob" title="Molecular Networks team">Molecular Networks team</a> (<a href="//www.ebi.ac.uk/" title="European Bioinformatics Institute">EMBL-EBI</a>, UK) and the <a href="http://www.sbml.org/" title="Systems Biology Markup Language (SBML)" class="external">SBML Team</a> (<a href="//www.caltech.edu/" title="California Institute of Technology">Caltech</a>, USA).

<div style="text-align:center; padding-top: 8px;">
  <a href="//www.ebi.ac.uk/" title="EMBL - European Bioinformatics Institute (EBI)"><img border="0" title="EMBL-EBI" alt="EMBL-EBI logo" src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/EMBL_EBI_logo-small.png" style="vertical-align:middle;" /></a>
  <a href="http://sbml.org/" title="SBML" style="padding-left:60px;"><img border="0" title="SBML" alt="SBML logo" src="//www.ebi.ac.uk/biomodels-static/icons/SBML.png" style="vertical-align:middle;" /></a>
</div>


<h3 class="underline">BioModels funders</h3>

<p>
  BioModels is supported by the <a href="//www.embl.org/" class="external">European Molecular Biology Laboratory</a>,
the <a href="http://www.bbsrc.ac.uk/" class="external">Biotechnology and Biological Sciences Research Council</a> (Multimod, BB/N019482/1) and
the <a href="//www.imi.europa.eu/" title="Innovative Medicines Initiative (IMI)">Innovative Medicines Initiative</a>
    (<a href="http://transqst.org/" title="This project has received funding from the Innovative Medicines Initiative 2 Joint Undertaking under grant agreement No 116030. This Joint Undertaking receives support from the European Union’s Horizon 2020 research and innovation programme and EFPIA.">TransQST, 116030</a>).
</p>

<div style="text-align:center; padding-top: 8px;">
  <a href="//www.embl.org/"
     title="European Molecular Biology Laboratory (EMBL)">
      <img src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/embl_logo.png"
           title="European Molecular Biology Laboratory (EMBL)"
           alt="EMBL logo" style="vertical-align: middle;" /></a>
  <a href="//www.bbsrc.ac.uk/"
     title="Biotechnology and Biological Sciences Research Council (BBSRC)">
      <img src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/bbsrc_logo.png"
           title="Biotechnology and Biological Sciences Research Council (BBSRC)"
           alt="BBSRC logo" style="padding-left:60px; vertical-align: middle;" /></a>
  <a href="//www.imi.europa.eu/"
     title="Innovative Medicines Initiative (IMI)">
      <img src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/IMI_logo-small.png"
           title="Innovative Medicines Initiative (IMI)"
           alt="IMI logo" style="padding-left:60px; vertical-align: middle;" /></a>
  <a href="//cordis.europa.eu/fp7/"
     title="Seventh Framework Programme (FP7)">
      <img src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/FP7_logo-small.png"
           title="Seventh Framework Programme (FP7)"
           alt="FP7 logo" style="padding-left:60px; vertical-align: middle;" /></a>
</div>

<h4>Former funders</h4>

<p>
  BioModels also benefited from the help of the <a href="//www.imi.europa.eu/" title="Innovative Medicines Initiative (IMI)">Innovative Medicines Initiative</a> (<a href="http://www.ddmore.eu/" title="Drug Disease Model Resources (DDMoRe)">DDMoRe</a>), the <a href="//cordis.europa.eu/fp7/" title="Seventh Framework Programme (FP7)">Seventh Framework Programme</a> (<a href="http://agedbrainsysbio.eu/" title="AgedBrainSYSBIO: Systems biology of pathways involved in brain ageing">AgedBrainSYSBIO</a>, <a href="http://community.isbe.eu/" title="Infrastructure for Systems Biology – Europe' (ISBE)">ISBE</a>), the <a href="http://www.nigms.nih.gov/" class="external">National Institute of General Medical Sciences</a> (USA), the <a href="http://www.sys-bio.org/" class="external">Sauro Lab</a> (University of Washington, USA), the <a href="http://sbi.jp/" class="external">Systems Biology Institute</a> (Tokyo, JP), and from funds of the <a href="http://www.darpa.mil/" class="external">DARPA</a> (USA).
</p>


<h3 class="underline">BioModels collaboration</h3>

<!--
<p>
   BioModels is developed in collaboration with (by alphabetical order): Andreas Dräger (<a href="http://www.uni-tuebingen.de/fakultaeten/mathematisch-naturwissenschaftliche-fakultaet/fachbereiche/zentren/zentrum-fuer-bioinformatik-tuebingen.html" title="Center for Bioinformatics (Zentrum für Bioinformatik - ZBIT) Tübingen" class="external">Center for Bioinformatics (ZBIT)</a>, University of Tübingen, DE), Jürgen Eils (<a href="www.dkfz.de/" title="German Cancer Research Center" class="external">DKFZ</a>, DE), Jacky Snoep (<a href="http://jjj.biochem.sun.ac.za/" title="JWS Online" class="external">JWS Online</a>, Stellenbosch University, ZA) and Neil Swainston (<a href="http://www.mcisb.org/" title="Manchester Centre for Integrative Systems Biology (MCISB)" class="external">Manchester Centre for Integrative Systems Biology</a>, University of Manchester, UK).
</p>
-->

<div style="text-align:center; padding-top: 8px;">
  <a href="http://www.uni-tuebingen.de/fakultaeten/mathematisch-naturwissenschaftliche-fakultaet/fachbereiche/zentren/zentrum-fuer-bioinformatik-tuebingen.html"
     title="Center for Bioinformatics (Zentrum für Bioinformatik - ZBIT) Tübingen">
      <img border="0"
           title="Center for Bioinformatics (Zentrum für Bioinformatik - ZBIT) Tübingen"
           alt="Center for Bioinformatics (Zentrum für Bioinformatik - ZBIT) Tübingen logo"
           src="//www.ebi.ac.uk/biomodels-static/icons/ZBIT_logo.png"
           style="vertical-align:middle;" /></a>
  <a href="http://www.dkfz.de/"
     title="DKFZ"
     style="padding-left:20px;">
      <img border="0"
           title="German Cancer Research Center (DKFZ)"
           alt="DKFZ logo" src="//www.ebi.ac.uk/biomodels-static/icons/dkfz_logo.png"
           style="vertical-align:middle;" /></a>
  <a href="http://jjj.biochem.sun.ac.za/"
     title="JWS online"
     style="padding-left:20px;">
      <img title="JWS Online"
           alt="JWS online logo"
           src="//www.ebi.ac.uk/biomodels-static/icons/JWS.png"
           style="vertical-align: middle; border: 0" /></a>
  <!--<a href="http://www.manchester.ac.uk/" title="University of Manchester" style="padding-left:20px;"><img border="0" title="University of Manchester" alt="University of Manchester logo" src="//www.ebi.ac.uk/biomodels/icons/Manchester-University_logo.png" style="vertical-align:middle;" /></a>-->
  <a href="http://www.mcisb.org/"
     title="MCISB"
     style="padding-left:20px;">
        <img title="Manchester Centre for Integrative Systems Biology (MCISB)"
             alt="MCISB logo"
             src="//www.ebi.ac.uk/biomodels-static/icons/MCISB_logo.png"
             style="vertical-align: middle; border: 0" /></a>
  <a href="//sems.uni-rostock.de/"
     title="Simulation Experiment Management for Systems Biology, Systems Biology and Bioinformatics, University of Rostock"
     style="padding-left:20px;">
      <img border="0" title="Simulation Experiment Management for Systems Biology"
           alt="SEMS logo"
           src="//www.ebi.ac.uk/biomodels-static/GRAPHICS/SEMS_logo.png"
           style="vertical-align:middle;" /></a>
</div>

<h4>Former collaborators</h4>

<p>
  BioModels collaborated with <a href="//doqcs.ncbs.res.in/" class="external">DOQCS</a> (National Center for Biological Sciences, India), <a href="//www.ccam.uchc.edu/" class="external">the Virtual Cell</a> (University of Connecticut Health Center, USA) and the <a href="//www.cellml.org" class="external">CellML Team</a> (Auckland Bioengineering Institute, NZ).
</p>

<p>
  BioModels would not exist without the continuous support of many people, whether by their contribution of models, of software, or by their constructive comments and criticisms. It is unfortunately impossible to acknowledge all of them here, without risking to be unfair. Therefore, a big collective thank-you all!
</p>

<!--
<h2>Individuals</h2>

<p>
  <strong>Model curators and annotators</strong>: Vijayalakshmi Chelliah and Nicolas Le Novère.
</p>
<p>
  <strong>Developers</strong>: Camille Laibe and Nicolas Rodriguez.
</p>
<p>
  <strong>Former model curators and annotators</strong>: Ishan Ajmera, Harish Dharuri, Lukas Endler, Enuo He, Lu Li, Rainer Machne, Maria Schilstra, Bruce Shapiro and Michael Schubert.
</p>
<p>
  <strong>Former developers</strong>: Mélanie Courtot, Marco Donizelli, Arnaud Henry, Gaël Jalowicki, Chen Li, Lu Li and Jean-Baptiste Pettit.
</p>
-->

<h3 class="underline">Scientific Advisory Board</h3>

<ul>
  <li>Carole Goble, <a href="http://www.manchester.ac.uk/" title="The University of Manchester">University of Manchester</a></li>
  <li>Thomas Lemberger, <a href="http://www.nature.com/" title="Nature Publishing Group">Nature Publishing Group</a>/<a href="http://www.embo.org/" title="European Molecular Biology Organization">EMBO</a></li>
  <li>Pedro Mendes, <a href="http://www.manchester.ac.uk/" title="The University of Manchester">University of Manchester</a></li>
  <li>Wolfgang Mueller, <a href="http://www.h-its.org/" title="Heidelberg Institute for Theoretical Studies (HITS)">HITS</a></li>
  <li>Philippe Sanseau, <a href="http://www.gsk.com/" title="GlaxoSmithKline">GSK</a></li>
</ul>

<h4>Former members</h4>

<ul>
  <li>Upinder S. Bhalla, National Center for Biological Sciences, Bangalore, IN</li>
  <li>Ion Moraru, University of Connecticut Health Center, USA</li>
  <li>Herbert Sauro, University of Washington, USA</li>
  <li>Jacky L. Snoep, Chair, University of Stellenbosch, SA</li>
</ul>


<br />

</body>
<content tag="acknowledgements">
    selected
</content>
<content tag="title">
    <g:message code="${titleCode}" default="Acknowledgements" />
</content>
