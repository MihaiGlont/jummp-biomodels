<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'publication')}" class="accordion-title">Publication Management</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Update publications via the publication editor</p>
        <a href="${createLink(controller: 'publication')}">Access service</a>
    </div>
</li>
