<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'homePage')}" class="accordion-title">Redis Cache Management</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Update data on Redis Server to change data showing on the home page</p>
        <a href="${createLink(controller: 'homePage')}">Access service</a>
    </div>
</li>
