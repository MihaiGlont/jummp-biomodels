<ul>
    <li><a href="${createLink(action: 'database', controller: "configuration")}">Database</a></li>
    <li><a href="${createLink(action: 'ldap', controller: "configuration")}">LDAP</a></li>
    <li><a href="${createLink(action: 'svn', controller: "configuration")}">Subversion</a></li>
    <li><a href="${createLink(action: 'vcs', controller: "configuration")}">Version Control System</a></li>
    <li><a href="${createLink(action: 'remote', controller: "configuration")}">Remote</a></li>
    <li><a href="${createLink(action: 'server', controller: "configuration")}">Server</a></li>
    <li><a href="${createLink(action: 'bives', controller: "configuration")}">Model Versioning System - BiVeS</a></li>
    %{--<li><a href="${createLink(action: 'userRegistration')}">User Registration</a></li>
    <li><a href="${createLink(action: 'changePassword')}">Change/Reset Password</a></li>--}%
    <li><a href="${createLink(action: 'branding', controller: "configuration")}">Select Branding</a></li>
    <li><a href="${createLink(action: 'cms', controller: "configuration")}">Content Management System</a></li>
    <li><a href="${createLink(action: 'classifier', controller: 'classifierConfigure', plugin: "jummp-plugin-biomodels-dom")}">Model Classifier System</a></li>
</ul>
