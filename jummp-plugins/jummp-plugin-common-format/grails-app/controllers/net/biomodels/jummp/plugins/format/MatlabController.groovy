/*
 * Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.plugins.format

import net.biomodels.jummp.core.IMetadataService
import net.biomodels.jummp.core.annotation.QualifierTransportCommand
import net.biomodels.jummp.core.annotation.ResourceReferenceTransportCommand
import net.biomodels.jummp.core.model.RevisionTransportCommand

class MatlabController {
    def matlabFormatService
    IMetadataService metadataDelegateService

    def show() {
        def model = flash.genericModel
        final RevisionTransportCommand revision = model.revision as RevisionTransportCommand
        Set<File> matlabFiles = matlabFormatService.getMatlabFilesFromRevision revision
        Map<QualifierTransportCommand, List<ResourceReferenceTransportCommand>> anno =
                metadataDelegateService.fetchGenericAnnotations(revision)

        model['annotations'] = anno
        model['matlabFiles'] = matlabFiles
        model
    }
}
