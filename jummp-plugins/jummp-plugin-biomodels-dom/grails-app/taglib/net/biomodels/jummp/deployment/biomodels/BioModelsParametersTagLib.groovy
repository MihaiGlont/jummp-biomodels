/**
 * Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.deployment.biomodels

/**
 * @short General purpose helper for rendering BioModels Parameter Search page.
 *
 * @author <a href="mailto:mihai.glont@ebi.ac.uk">Mihai Glonț</a>
 * @author <a href="mailto:tung.nguyen@ebi.ac.uk">Tung Nguyen</a>
 */
class BioModelsParametersTagLib {
    static defaultEncodeAs = [taglib: 'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = 'bp'

    Closure renderText = {
        out << "Showing "
    }

    Closure renderSboTerms =  { attrs ->
        def result = ''
        if (attrs.termArray) {
            result = render(template: "/templates/parameters/bpSboTerms",
                            plugin: "jummp-plugin-biomodels-dom", model: [termArray: attrs.termArray])
        } else
            out << result
    }
}
