<p>BioModels Parameters is a resource that facilitates easy search and retrieval of parameter values used in the SBML
models stored in the BioModels repository. Users can search for a model entity (e.g. a protein or drug) to retrieve
the rate equations describing it; the associated parameter values and the initial concentration from the SBML models
in BioModels. Although these data are directly extracted from the curated SBML models, they are not individually
curated or validated; rather presented as such in the table below.
Hence BioModels Parameters table will only provide a quick overview of available parameter values for guidance and
original model should be referred to understand the complete context of the parameter usage.</p>
