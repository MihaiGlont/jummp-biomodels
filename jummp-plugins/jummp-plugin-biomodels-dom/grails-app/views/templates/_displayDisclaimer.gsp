<div class="row">
    <div class="small-12 medium-6 large-4 columns">
        <img src="https://img.icons8.com/office/80/000000/error.png"
             width="50%" class="float-center">
    </div>
    <div class="small-12 medium-6 large-8 columns">
        <span style="font-weight: bolder">${message}</span>
    </div>
</div>
