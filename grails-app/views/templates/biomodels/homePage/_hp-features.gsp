<div class="homepage_info_box show-for-small-only"><h3>Highlight Features</h3></div>
<div id="hp-features-data-submission-update" class="large-2 medium-2 small-12 columns">
    <biomd:renderHomePageFeaturesDataSubmission/>
</div>
<div id="hp-features-browse-models" class="large-8 medium-8 small-12 columns">
    <biomd:renderHomePageFeaturesModelBrowse/>
</div>
<div id="hp-features-parameters-search" class="large-2 medium-2 small-12 columns">
    <biomd:renderHomePageFeaturesParametersSearch/>
</div>
