<div class="row">
    <div class="large-12 medium-12 small-12">
        <a href="${createLink(controller: "parameterSearch", action: "index")}"
           title="Search BioModels Parameters of interest"><span
            class="size-400 icon icon-common icon-sliders-h icon-spacer hide-for-small-only"></span></a>
        <h4><a href="${createLink(controller: "parameterSearch", action: "index")}"
               title="Search BioModels Parameters of interest">BioModels Parameters</a></h4>
    </div>
</div>
<div class="row">
    <div class="large-12 medium-12 small-12">
        <h5>${totalRecords} records</h5>
    </div></div>
