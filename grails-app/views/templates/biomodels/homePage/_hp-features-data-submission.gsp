<a href="${createLink(controller: "model", action: "create")}"
   title="Go to submission or update flow">
    <span class="size-400 icon icon-common icon-spacer hide-for-small-only" data-icon="&#xf093;"></span></a>
<h4><a href="${createLink(controller: "model", action: "create")}" title="Go to submission or update flow">
    Submission / Update</a></h4>
